package com.automation.database;

//import org.apache.commons.lang3.RandomStringUtils;

import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DataProviderClass extends DatabaseMethods {

    @DataProvider
    public Object[][] dataForSearch() throws SQLException, IOException {
        ResultSet resultSet = selectQuery("SELECT city FROM worldOfAutomation.address;");
        String x = getValue(resultSet, "city");

        String y = RandomStringUtils.randomAlphabetic(7);
        System.out.println(y);

        return new Object[][]{{x}, {y}};
    }
}