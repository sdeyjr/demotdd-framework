package com.automation.database;

import com.automation.core.ExtentTestManager;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class DatabaseMethods extends DatabaseConnection {

    public static ResultSet selectQuery(String query) throws IOException, SQLException {
        connectionToSQL();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        return resultSet;
    }

    public static String getValue(ResultSet resultSet, String columnName) throws SQLException {
        String name = null;
        while (resultSet.next()) {
            name = resultSet.getString(columnName);
            System.out.println(resultSet.getString(columnName));
        }
        return name;
    }

    public static Map<String, String> getMapFromDBOfSingleRecord(String query) throws SQLException, IOException {
        connectionToSQL();
        ExtentTestManager.log("connection succesfull");
        Map<String, String> data = new HashMap<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            ResultSetMetaData rsMetaData = resultSet.getMetaData();

            while (resultSet.next()) {
                for (int i = 1; i <= rsMetaData.getColumnCount(); i++) {
                    String value = null;
                    if (value == null) {
                        value = resultSet.getString(rsMetaData.getColumnName(i));
                        data.put(rsMetaData.getColumnName(i), value);
                    }
                }
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        return data;
    }

    public static ArrayList<Map> getListOfMapsFromDBOfSingleRecord(String query) throws SQLException, IOException {
        connectionToSQL();
        ExtentTestManager.log("connection succesfull");
        ArrayList<Map> listOfData = new ArrayList<>();

        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            ResultSetMetaData rsMetaData = resultSet.getMetaData();

            while (resultSet.next()) {
                Map<String, String> singleData = new HashMap<>();
                for (int i = 1; i <= rsMetaData.getColumnCount(); i++) {
                    String value = null;
                    if (value == null) {
                        value = resultSet.getString(rsMetaData.getColumnName(i));
                        singleData.put(rsMetaData.getColumnName(i), value);
                    }
                }
                listOfData.add(singleData);
            }

        } catch (Exception ee) {
            ee.printStackTrace();
        }
        return listOfData;
    }

    public void validateTestRunForAddress() throws SQLException, IOException {
        Map<String, String> data = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.address");
        System.out.println(data);
    }

    public void validateTestRunForEmployees() throws SQLException, IOException {
        Map<String, String> data2 = getMapFromDBOfSingleRecord("SELECT * FROM worldOfAutomation.employee");
        System.out.println(data2);
    }
}
