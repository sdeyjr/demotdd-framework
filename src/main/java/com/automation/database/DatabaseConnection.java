package com.automation.database;

import com.automation.core.TestBase;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DatabaseConnection extends TestBase {

    public static Connection connection;

    public static void connectionToSQL() throws IOException, SQLException {
        String url = getPropertyFile("src/main/resources/configure.properties", "My_SQL_URL");
        String username = getPropertyFile("src/main/resources/configure.properties", "My_SQL_Username");
        String password = getPropertyFile("src/main/resources/configure.properties", "My_SQL_Password");
        connection = DriverManager.getConnection(url, username, password);
    }

}