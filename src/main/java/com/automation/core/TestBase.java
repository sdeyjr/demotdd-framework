package com.automation.core;


import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import static com.automation.core.ExtentTestManager.*;

public class TestBase {
    public static WebDriver driver;
    private static ExtentReports extent;

    @BeforeMethod
    @Parameters({"browserName", "url", "cloud", "os", "cloudOptions"})
    public void setupBrowser(String browserName, String url, boolean cloud, String os, String cloudOptions) throws IOException {
        if (cloud) {
            if (cloudOptions.equalsIgnoreCase("BrowserStack")) {
                driver = getRemoteWebDriverBrowserStack();
            } else {
                driver = getRemoteWebDriverSauceLab();
            }
        } else {
            driver = getLocalWebDriver(browserName, os);
        }
        driver.get(url);
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterMethod
    public void quitBrowser() {
        driver.quit();
    }

    private WebDriver getLocalWebDriver(String browserName, String os) {
        WebDriver driver;
        if (browserName.equalsIgnoreCase("Chrome")) {
            if (os.equalsIgnoreCase("mac")) {
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");
            } else {
                System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
            }
            driver = new ChromeDriver();
        } else {
            if (os.equalsIgnoreCase("mac")) {
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");
            } else {
                System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver.exe");
            }
            driver = new FirefoxDriver();
        }
        return driver;
    }

    private WebDriver getRemoteWebDriverSauceLab() throws IOException {
        ChromeOptions browserOptions = new ChromeOptions();
        browserOptions.setPlatformName("macOS 12");
        browserOptions.setBrowserVersion("98");

        Map<String, Object> sauceOptions = new HashMap<>();
        browserOptions.setCapability("sauce:options", sauceOptions);

        String url = getPropertyFile("src/main/resources/configure.properties", "Saucelab_URL");
        WebDriver driver = new RemoteWebDriver(new URL(url), browserOptions);
        return driver;
    }

    private WebDriver getRemoteWebDriverBrowserStack() throws IOException {
        FileInputStream fis = new FileInputStream("src/main/resources/configure.properties");
        Properties prop = new Properties();
        prop.load(fis);

        String bsKeys = prop.getProperty("BROWSERSTACK_USERNAME");
        String bsAccessKeys = prop.getProperty("BROWSERSTACK_ACCESS_KEY");
        final String URL_OF_BS = "https://" + bsKeys + ":" + bsAccessKeys + "@hub-cloud.browserstack.com/wd/hub";

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("browserName", "chrome");
        capabilities.setCapability("browserVersion", "98");

        HashMap<String, Object> browserstackOptions = new HashMap<>();
        browserstackOptions.put("os", "OS X");
        browserstackOptions.put("osVersion", "Monterey");

        capabilities.setCapability("bstack:options", browserstackOptions);

        WebDriver driver = new RemoteWebDriver(new URL(URL_OF_BS), capabilities);
        return driver;
    }

    public static void waitFor(int seconds) {
        try {
            // static sleep
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void scrollDownToSpecificElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].scrollIntoView(true);", element);
        waitFor(5);
    }

    public static String getPropertyFile(String filepath, String key) throws IOException {
        Properties properties = new Properties();
        properties.load(new FileInputStream(filepath));
        return properties.getProperty(key);
    }

    //Extent Report Setup
    @BeforeSuite(alwaysRun = true)
    public void extentSetup(ITestContext context) {
        ExtentTestManager.setOutputDirectory(context);
        extent = ExtentTestManager.getInstance();
    }

    //Extent Report Setup for each methods
    @BeforeMethod(alwaysRun = true)
    public void startExtent(Method method) {
        String className = method.getDeclaringClass().getSimpleName();
        ExtentTestManager.startTest(method.getName());
        ExtentTestManager.getTest().assignCategory(className);
    }

    //Extent Report cleanup for each methods
    @AfterMethod(alwaysRun = true)
    public void afterEachTestMethod(ITestResult result) {
        ExtentTestManager.getTest().getTest().setStartedTime(getTime(result.getStartMillis()));
        ExtentTestManager.getTest().getTest().setEndedTime(getTime(result.getEndMillis()));
        for (String group : result.getMethod().getGroups()) {
            ExtentTestManager.getTest().assignCategory(group);
        }

        if (result.getStatus() == 1) {
            ExtentTestManager.getTest().log(LogStatus.PASS, "Test Case Passed : " + result.getName());
        } else if (result.getStatus() == 2) {
            ExtentTestManager.getTest().log(LogStatus.FAIL, "Test Case Failed : " + result.getName() + " :: " + getStackTrace(result.getThrowable()));
        } else if (result.getStatus() == 3) {
            ExtentTestManager.getTest().log(LogStatus.SKIP, "Test Case Skipped : " + result.getName());
        }
        ExtentTestManager.endTest();
        extent.flush();
        if (result.getStatus() == ITestResult.FAILURE) {
            captureScreenshot(driver, result.getName());
        }
    }

    //Extent Report closed
    @AfterSuite(alwaysRun = true)
    public void generateReport() {
        extent.close();
    }
}