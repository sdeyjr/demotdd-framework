package com.automation.pages;

import com.automation.core.ExtentTestManager;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class AmazonHomePage {

    @FindBy(id = "twotabsearchtextbox")
    private WebElement searchBox;

    @FindBy(id = "nav-search-submit-button")
    private WebElement searchButton;

    @FindBy(id = "nav-link-accountList")
    private WebElement signInOption;

    public void typeOnSearchBar(String data) {

        Assert.assertTrue(searchBox.isDisplayed());
        ExtentTestManager.log("Searchbox is displayed");

        searchBox.clear();
        ExtentTestManager.log("Searchbox has been cleared");

        searchBox.sendKeys(data);
        ExtentTestManager.log("Data has been entered");
    }

    public void submitSearch() {

        Assert.assertTrue(searchButton.isDisplayed());
        ExtentTestManager.log("Search button is displayed");

        searchButton.click();
        ExtentTestManager.log("Search button has been clicked");
    }

}
