package com.automation.pages;

import com.automation.core.ExtentTestManager;
import com.automation.core.TestBase;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

public class NikeHomePage {

    @FindBy(id = "hf_title_signin_membership")
    private WebElement signInLink;

    @FindBy(xpath = "//div[@class='view-header']")
    private WebElement signInPageHeader;

    @FindBy(xpath = "(//a[@href='https://www.nike.com/w/sale-3yaep'])[2]")
    private WebElement salesOption;

    @FindBy(xpath = "//a[@aria-label='Selected Location: US']")
    private WebElement countryOption;

    @FindBy(xpath = "//a[@lang='en-CA']")
    private WebElement canadaOption;

    @FindBy(xpath = "//span[@class='country-pin-label']")
    private WebElement selectedRegionHeader;

    @FindBy(id = "hf-geoselection-title")
    private WebElement countryOptions;

    @FindBy(xpath = "(//a[@href='https://www.nike.com/ca/w/womens-sale-shoes-3yaepz5e1x6zy7ok'])[2]")
    private WebElement womensShoeSale;

    public void validateUserCanClickSignIn() {
        signInLink.click();
        ExtentTestManager.log("Sign-in button has been clicked");
    }

    public void validateSignInPopUpLoaded() {
        String data = signInPageHeader.getText();
        Assert.assertEquals("YOUR ACCOUNT FOR EVERYTHING NIKE", data, "Sign-in page didn't load properly");
        ExtentTestManager.log("Sign-in pop up has been loaded");
    }

    public void validateUserCanGoToWomensShoeSales() {
        Actions action = new Actions(TestBase.driver);
        action.moveToElement(salesOption).build().perform();
        TestBase.waitFor(3);
    }

    public void validateUserCanClickOnCountryOption() {
        countryOption.click();
        ExtentTestManager.log("Country option has been clicked");
    }

    public void validateSelectCountryOptionsLoaded() {
        String rv6 = countryOptions.getText();
        Assert.assertEquals(rv6, "Select your Location");
        ExtentTestManager.log("Region option has been loaded");
    }

    public void validateUserCanClickOnCanada() {
        canadaOption.click();
        ExtentTestManager.log("Canada has been clicked");
    }

    public void validateUserCanSeeCanadianSite() {
        String currentUrl = TestBase.driver.getCurrentUrl();
        Assert.assertTrue(currentUrl.contains("ca"));
        ExtentTestManager.log("Region has been switched");

        String rv1 = selectedRegionHeader.getText();
        Assert.assertTrue(selectedRegionHeader.isDisplayed());
        Assert.assertEquals("Canada", rv1, "Canadian site didn't load");
        ExtentTestManager.log("Canadian site has been loaded");
    }
}
