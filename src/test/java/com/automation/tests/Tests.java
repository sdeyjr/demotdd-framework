package com.automation.tests;

import com.automation.core.TestBase;
import com.automation.database.DataProviderClass;
import com.automation.database.DatabaseMethods;
import com.automation.pages.AmazonHomePage;
import com.automation.pages.NikeHomePage;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.sql.SQLException;

public class Tests extends TestBase {

    private NikeHomePage nikeHomePage;
    private AmazonHomePage amazonHomePage;

    //*** Website mentioned in the runner file ***

    @BeforeMethod
    public void instances() {
        nikeHomePage = PageFactory.initElements(driver, NikeHomePage.class);
        amazonHomePage = PageFactory.initElements(driver, AmazonHomePage.class);
    }

    @Test(enabled = false)
    public void validateUserCanNavigateToSignInPage() {
        nikeHomePage.validateUserCanClickSignIn();
        nikeHomePage.validateSignInPopUpLoaded();
    }

    @Test(enabled = false)
    public void validateUserCanSwitchRegions() {
        nikeHomePage.validateUserCanClickOnCountryOption();
        nikeHomePage.validateSelectCountryOptionsLoaded();
        nikeHomePage.validateUserCanClickOnCanada();
        nikeHomePage.validateUserCanSeeCanadianSite();
    }

    @Test(enabled = false)
    public void validateUserCanGoToWomensShoeSale() {
        nikeHomePage.validateUserCanGoToWomensShoeSales();
    }

    @Test(enabled = false)
    public void testDBConnection() throws SQLException, IOException {
        DatabaseMethods databaseMethods = new DatabaseMethods();
        databaseMethods.validateTestRunForAddress();
        databaseMethods.validateTestRunForEmployees();
    }

    @Test(dataProvider = "dataForSearch", dataProviderClass = DataProviderClass.class, enabled = true)
    public void validateUserCanSearchForItems(String data) {
        amazonHomePage.typeOnSearchBar(data);
        amazonHomePage.submitSearch();
    }

}
